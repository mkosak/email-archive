import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { EmailsListComponent } from './components/emails-list/emails-list.component';
import { EmailDetailComponent } from './components/email-detail/email-detail.component';
import { SearchFiltersComponent } from './components/search-filters/search-filters.component';

import { NoSubjectPipe } from './shared/no-subject.pipe';
import { HighlightSearchPipe } from './shared/highlight.pipe';
import { FormatLineBreaksPipe } from './shared/format-line-breaks.pipe';

@NgModule({
  declarations: [
    AppComponent,
    EmailsListComponent,
    EmailDetailComponent,
    SearchFiltersComponent,
    NoSubjectPipe,
    HighlightSearchPipe,
    FormatLineBreaksPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
