import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { Email } from '../components/emails-list/email';
import { environment } from './../../environments/environment';

@Injectable({ providedIn: 'root' })
export class EmailsService {
  private emailsUrl = environment.dataUrl;  // URL to fetch

  constructor(private http: HttpClient) { }

  /**
   * Fetch emails from the server
   * @method
   */
  fetchEmails(): Observable<Email[]> {
    return this.http.get<Email[]>(this.emailsUrl)
      .pipe(
        // modify response data
        map(res => {
          // loop and prepare data
          return res.reduce((acc: any, current, idx, arr) => {
            // add new property 'subjectBody' to merge subject and body allowing table sort
            current.subjectBody = `${current.subject} ${current.body}`;

            // add new property 'persons' to collect all persons emails
            current.persons = [...current.to, current.cc, current.bcc, current.from].flat().join(',');

            // collect emails threads and add them as a new "threads" property
            const threads: any = [];

            // check for non replys subject
            const currentSubject: any = current.subject;
            let noReplySubject: any = (currentSubject.toLowerCase().indexOf('re:') === -1 &&
                                       currentSubject.toLowerCase().indexOf('fw:') === -1 &&
                                       currentSubject !== '') ? currentSubject.toLowerCase() : false;

            // compare current email with the rest in the array
            arr.forEach((email) => {
              const subject: any = email.subject.toLowerCase();
              let replySubject: any = (subject.indexOf('re:') === 0 || subject.indexOf('fw:') === 0) ? subject : false;

              // if it is a reply subject extract the subject name
              if (replySubject) {
                replySubject = (replySubject.indexOf('re:') === 0) ? replySubject.split('re:') : replySubject.split('fw:');
                replySubject = replySubject.length > 1 ? replySubject[1].substring(1) : null;

                // find related emails for each non reply item
                if (replySubject && replySubject === noReplySubject) {
                  threads.push(email);
                }
              }
            });

            // if replys emails exist add them to the newly created property
            if (threads.length) {
              current.threads = threads.sort((a: Email, b: Email) => a.date < b.date);
            }

            acc.push(current);

            return acc;
          }, []);
        }),
        catchError(this.handleError<Email[]>('getEmails', []))
      );
  }

  /**
   * Handling errors
   * @func
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // log error to console
      console.error(error);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /**
   * Log the errors
   * @func
   */
  private log(message: string) {
    console.log(`EmailService: ${message}`);
  }
}
