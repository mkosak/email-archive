export interface Email {
  from: string;
  to: Array<string>;
  cc: Array<string>;
  bcc: Array<string>;
  persons: string;
  subject: string;
  subjectBody: string;
  body: string;
  date: string;
  threads: Array<Email>;
  hideInList: boolean;
}
