import { OnInit, Component, ViewChild } from '@angular/core';

import { Email } from './email';
import { EmailsService } from '../../services/emails.service';

import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-emails-list',
  templateUrl: './emails-list.component.html',
  styleUrls: ['./emails-list.component.scss']
})
export class EmailsListComponent implements OnInit {
  isLoading = true;
  emails: Email[] = [];
  selectedEmail?: Email;
  showDetailed = false;
  searchTerm = '';

  displayedColumns: string[] = ['from', 'subjectBody', 'date'];
  dataSource = new MatTableDataSource();

  constructor(private emailsService: EmailsService) {}

  @ViewChild(MatSort) sort: MatSort | any;
  @ViewChild(MatPaginator) paginator: MatPaginator | any;

  ngOnInit(): void {
    // initiate data request
    this.getAllEmails();

    // configure custom filter for the table
    this.dataSource.filterPredicate = this.customFilterPredicate;
  }

  /**
   * Create custom filter predicate for mat-table
   * @func
   */
  customFilterPredicate(data: any, filter: any): boolean {
    const a = !filter.person ||
          data.persons.includes(filter.person);
    const b = !filter.term ||
          data.subject.toLowerCase().includes(filter.term) ||
          data.body.toLowerCase().includes(filter.term);
    const c = !filter.date ||
          data.date.includes(filter.date.split('T') &&
          filter.date.split('T')[0]);

    return a && b && c;
  }

  /**
   * Open email detail
   * @method
   */
  openDetailed(email: Email): void {
    this.selectedEmail = email;
    this.showDetailed = true;
  }

  /**
   * Close email detail
   * @method
   */
  closeDetailed(): void {
    this.showDetailed = false;
  }

  /**
   * Rest mat-table filter
   * @method
   */
  resetFilters(): void {
    this.dataSource.filter = '';
  }

  /**
   * Apply mat-table filter
   * @method
   */
  applyFilters(filter: any): void {
    this.showDetailed = false;
    this.dataSource.filter = filter;
    this.searchTerm = filter.term;
  }

  /**
   * Call data fetching service method
   * @method
   */
  getAllEmails(): void {
    this.emailsService.fetchEmails()
    .subscribe(res => {
      // loop through the threads and collect their subjects
      const subjectsSet = new Set();
      res.map((email) => {
        if (email.threads && email.threads.length) {
          email.threads.forEach((thread) => subjectsSet.add(thread.subject.toLocaleLowerCase()));
        }
      });
      const subjectsToHide = Array.from(subjectsSet);

      // filter emails by showing only the main email in a thread
      const filteredEmails = res.filter((email) => {
        return subjectsToHide.indexOf(email.subject.toLocaleLowerCase()) === -1;
      });

      // set received data
      this.dataSource.data = filteredEmails as Email[];
      this.isLoading = false;

      // apply sorting and pagination after view init
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }
}
