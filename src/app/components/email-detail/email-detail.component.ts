import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Email } from '../emails-list/email';

@Component({
  selector: 'app-email-detail',
  templateUrl: './email-detail.component.html',
  styleUrls: ['./email-detail.component.scss']
})
export class EmailDetailComponent {
  @Input() email: Email | any;
  @Input() toggle = false;
  @Input() searchTerm: string | any;
  @Output() closeDetailed = new EventEmitter();

  toggleView(): void {
    this.toggle = !this.toggle;
    this.closeDetailed.emit(this.toggle);
  }
}
