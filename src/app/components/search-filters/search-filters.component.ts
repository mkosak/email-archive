import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, AbstractControl } from '@angular/forms';

import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-search-filters',
  templateUrl: './search-filters.component.html',
  styleUrls: ['./search-filters.component.scss']
})
export class SearchFiltersComponent {
  @Output() resetFilters = new EventEmitter();
  @Output() applyFilters = new EventEmitter();

  readonly formControl: AbstractControl | any;
  dataSource = new MatTableDataSource();

  constructor(formBuilder: FormBuilder) {
    // init form properties
    this.formControl = formBuilder.group({
      person: '',
      term: '',
      date: '',
    });

    // subscribe to form events
    this.formControl.valueChanges.subscribe((value: any) => {
      // combine filters
      const filter = {
        ...value,
        person: value.person && value.person.trim().toLowerCase(), // prepare person string
        term: value.term && value.term.trim().toLowerCase(), // prepare search term string
        date: value.date && value.date.toLocaleDateString('en-CA') // format date
      } as string;

      // apply filters change
      this.doFilter(filter);
    });
  }

  /**
   * Apply filters event
   * @method
   */
  doFilter(filter: string): void {
    this.applyFilters.emit(filter);
  }

  /**
   * Emit reset event
   * @method
   */
  doReset(): void {
    this.resetFilters.emit();
  }
}
