import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'noSubject'
})
export class NoSubjectPipe implements PipeTransform {
  transform(value: string): string {
    return value ? value : '(no subject)';
  }
}
